use askama::Template;

use actix_web::{HttpRequest, Responder};

use crate::AppState;

#[derive(Template)]
#[template(path = "index.html")]
struct Home {}

pub fn home(_req: &HttpRequest<AppState>) -> impl Responder {
    Home {}
}
