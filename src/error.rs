use crate::AppState;
use actix_web::{HttpRequest, Responder, Result};

use askama::Template;

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorPage {
    title: String,
}

impl ErrorPage {
    pub fn new(title: String) -> Self {
        Self { title }
    }
}

pub fn e404(_req: &HttpRequest<AppState>) -> Result<impl Responder> {
    Ok(ErrorPage::new(String::from("404: Page Not Found")))
}
