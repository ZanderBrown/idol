use env_logger;
use log::info;

mod error;
mod home;

use crate::error::e404;
use crate::home::home;

use actix_web::middleware::Logger;
use actix_web::{fs, http, pred, server, App, HttpResponse};

#[derive(Debug, Clone)]
pub struct AppState {}

fn main() {
    info!("Starting Server");
    env_logger::init();

    server::new(|| {
        let state = AppState {};
        App::with_state(state)
            .middleware(Logger::default())
            .resource("/", |r| r.f(home))
            .handler("/assets/", fs::StaticFiles::new("./assets").unwrap())
            .default_resource(|r| {
                r.method(http::Method::GET).f(e404);
                r.route()
                    .filter(pred::Not(pred::Get()))
                    .f(|_| HttpResponse::MethodNotAllowed());
            })
    })
    .bind("127.0.0.1:3000")
    .expect("Can not bind to port 3000")
    .run();
}
